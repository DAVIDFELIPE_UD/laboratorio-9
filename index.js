import express from 'express';


const app = express();
let puerto=3001;

//define una ruta, punto de acceso
app.get("/Bienvenida", (req,res)=>{ //funciones anonimas. puerto 3001
    res.send("hola mundo querido, podras acceder a la calculadora si vas al puerto 3000 hasta el 2996. Tendras la funcion de /sumar, /restar, /multiplicar, /dividir y /modulo");
}
);

app.get("/sumar", function(peticion,respuesta){ //funciones anonimas. puerto 3000
    let resultado=Number(peticion.query.a)+Number(peticion.query.b);
    respuesta.send(resultado.toString());
}
);

app.get("/restar", function(pet,resp){ //funciones anonimas. puerto 2999
    let resultado=Number(pet.query.a)-Number(pet.query.b);
    resp.send(resultado.toString());
}
);

app.get("/multiplicar", function(pe,re){ //funciones anonimas. puerto 2998
    let resultado=Number(pe.query.a)*Number(pe.query.b);
    re.send(resultado.toString());
}
);

app.get("/dividir", function(p,r){ //funciones anonimas. puerto 2997
    let resultado=Number(p.query.a)/Number(p.query.b);
    if(Number(p.query.b)!=0){
    r.send(resultado.toString());
    }
    else r.send("ingrese un valor distinto de cero para el denominador");
}
);

app.get("/modulo", function(p,r){ //funciones anonimas. puerto 2996
    let resultado=Number(p.query.a)%Number(p.query.b);
    r.send(resultado.toString());
}
);

//iniciar un servidor, un servicio que permita escuchar al computador
app.listen(puerto, function (){

    console.log("el servidor escuchando en el puerto " +puerto); //imprime en el terminal 
})
